A = 0.035;
B = 0.035;
C = 0.007;
I = 0.000001872;
J = 0;
tau = 0.01;
x_1 = 0;
x_2 = 0;
x_3 = 0;
u_1 = 250;
u_2 = 250;
u_3 = 250;
A_matrix = [0  ((B - C)*x_3 - I*u_3)*(I + A + 2*J)^-1 ((B - C)*x_2 - I*u_2)*(I + A + 2*J)^-1; ((C - A)*x_3 - I*u_3)*(I + B + 2*J)^-1 0 ((C - A)*x_1 - I*u_1)*(I + B + 2*J)^-1;((A - B)*x_2 - I*u_2)*(I + C + 2*J)^-1 ((A - B)*x_1 - I*u_1)*(I + C + 2*J)^-1 0];

B_matrix = [(tau^-1)*(I/(I + A + 2*J)) x_3*I/(I + A + 2*J) -x_2*I/(I + A + 2*J); -x_3*I/(I + B + 2*J) (tau^-1)*I/(I + B + 2*J) x_1*I/(I + B + 2*J);x_2*I/(I + C + 2*J) -x_1*I/(I + C + 2*J) (tau^-1)*I/(I + C + 2*J)];

C_matrix = [1 0 0; 0 1 0; 0 0 1];


digits(4)
A_m_num_display = vpa(A_matrix)
B_m_num_display = vpa(B_matrix)

R = [B_m_num_display A_m_num_display*B_m_num_display (A_m_num_display^2)*B_m_num_display];
R_num = vpa(R)
rank(R_num)

s = sym('s');
H = C_matrix*(s*eye(3) - A_matrix)^{-1}*B_matrix;
digits(2)
H_display = vpa(H)

